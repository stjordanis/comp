# COMP

Compare files and display their differences


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## COMP.LSM

<table>
<tr><td>title</td><td>COMP</td></tr>
<tr><td>version</td><td>1.04a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-10-21</td></tr>
<tr><td>description</td><td>Compare files and display their differences</td></tr>
<tr><td>keywords</td><td>compare, comp, file compare</td></tr>
<tr><td>author</td><td>Paul Vojta &lt;vojta -at- math.berkeley.edu&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Paul Vojta &lt;vojta -at- math.berkeley.edu&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT license](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Comp</td></tr>
</table>
